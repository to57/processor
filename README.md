# Processor Checkpoint for ECE 350
## Thomas Owens (to57)

Things that my processor does not do:
- Bypass $rstatus to a bex instruction correctly when it is written to by an exception (works correctly with setx)
- Set exception status register differently depending on the operation that caused the exception.
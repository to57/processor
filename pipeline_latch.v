module pipeline_latch(
    pc_d, ir_d, dataA_d, dataB_d, clock, 
    pc_q, ir_q, dataA_q, dataB_q, reset, stall
    );

    input [31:0] pc_d, ir_d, dataA_d, dataB_d;
    input clock, reset, stall;

    output [31:0] pc_q, ir_q, dataA_q, dataB_q;

    wire we;
    assign we = (~reset & ~stall);

    register32 pc_reg(pc_d, we, pc_q, clock, reset);
    register32 ir_reg(ir_d, we, ir_q, clock, reset);
    register32 dataA_reg(dataA_d, we, dataA_q, clock, reset);
    register32 dataB_reg(dataB_d, we, dataB_q, clock, reset);

endmodule
/**
 * READ THIS DESCRIPTION!
 *
 * This is your processor module that will contain the bulk of your code submission. You are to implement
 * a 5-stage pipelined processor in this module, accounting for hazards and implementing bypasses as
 * necessary.
 *
 * Ultimately, your processor will be tested by a master skeleton, so the
 * testbench can see which controls signal you active when. Therefore, there needs to be a way to
 * "inject" imem, dmem, and regfile interfaces from some external controller module. The skeleton
 * file, Wrapper.v, acts as a small wrapper around your processor for this purpose. Refer to Wrapper.v
 * for more details.
 *
 * As a result, this module will NOT contain the RegFile nor the memory modules. Study the inputs 
 * very carefully - the RegFile-related I/Os are merely signals to be sent to the RegFile instantiated
 * in your Wrapper module. This is the same for your memory elements. 
 *
 *
 */
module processor(
    // Control signals
    clock,                          // I: The master clock
    reset,                          // I: A reset signal

    // Imem
    address_imem,                   // O: The address of the data to get from imem
    q_imem,                         // I: The data from imem

    // Dmem
    address_dmem,                   // O: The address of the data to get or put from/to dmem
    data,                           // O: The data to write to dmem
    wren,                           // O: Write enable for dmem
    q_dmem,                         // I: The data from dmem

    // Regfile
    ctrl_writeEnable,               // O: Write enable for RegFile
    ctrl_writeReg,                  // O: Register to write to in RegFile
    ctrl_readRegA,                  // O: Register to read from port A of RegFile
    ctrl_readRegB,                  // O: Register to read from port B of RegFile
    data_writeReg,                  // O: Data to write to for RegFile
    data_readRegA,                  // I: Data from port A of RegFile
    data_readRegB                   // I: Data from port B of RegFile
	 
	);

	// Control signals
	input clock, reset;
	
	// Imem
    output [31:0] address_imem;
	input [31:0] q_imem;

	// Dmem
	output [31:0] address_dmem, data;
	output wren;
	input [31:0] q_dmem;

	// Regfile
	output ctrl_writeEnable;
	output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	output [31:0] data_writeReg;
	input [31:0] data_readRegA, data_readRegB;

	/* YOUR CODE STARTS HERE */

    wire writeReg_select, aluB_select, dmemD_bypass_select, is_imm_dx, is_sw_decode, rd_zero_mw, rd_zero_xm, 
         is_sw_mw, is_sw_xm, stall, is_lw_dx, is_sw_fd, is_r_type_dx, alu_overflow, ctrl_MULT, ctrl_DIV, 
         md_exception, md_resultRDY, md_stall, resultRDY_shift_half, md_op_finished, is_blt_dx,
         alu_lt, alu_neq, do_blt, flush_fd, flush_dx, is_branch_fd, do_bne, is_bex_fd, do_bex, is_setx_dx,
         write_rstatus, do_jump, is_jr_fd, do_jr, do_jal, is_sw_dx, rd_zero_dx, is_branch_dx, flush_xm, is_branch_mw, 
         is_branch_xm, is_setx_xm;
    wire [1:0] aluA_bypass_select, aluB_bypass_select, readRegB_bypass_select;
    wire [4:0] alu_shamt, alu_opcode, opcode_dx, opcode_mw, opcode_fd, opcode_xm;
    wire [16:0] imm_17_dx, imm_17_fd;
    wire [26:0] imm_27_dx, imm_27_fd;
    wire [31:0] pc_inc, alu_out, alu_dopB, sx_imm_17_dx, sx_imm_27_dx, X_opA, X_opB, ir_pre_dxreg, 
                ir_pre_xmreg, dataA_pre_xmreg, ir_pre_fdreg, next_pc, sx_imm_17_fd, dA_pre_dxreg,
                md_product, sx_imm_27_fd, data_bypassed_readRegB, bypassed_rstatus,
                pc_pcreg, pc_fdreg, pc_dxreg, 
                          ir_fdreg, ir_dxreg, ir_xmreg, ir_mwreg, ir_pwreg,
                                    da_dxreg, da_xmreg, da_mwreg, da_pwreg,
                                    db_dxreg, db_xmreg, db_mwreg,
                pc_blt, pc_bne, pc_jump, pc_jr;

    /* ----- Pipeline Latches----- */

    pipeline_latch PC(.pc_d(next_pc), .pc_q(pc_pcreg), .clock(clock), // Unused in/outs left empty
                        .ir_d(), .ir_q(), .dataA_d(), .dataB_d(), .dataA_q(), .dataB_q(), .reset(reset), .stall(stall));
    pipeline_latch FD(.pc_d(pc_inc), .pc_q(pc_fdreg), .ir_d(ir_pre_fdreg), .ir_q(ir_fdreg), .clock(clock),  
                        .dataA_d(), .dataB_d(), .dataA_q(), .dataB_q(), .reset(reset), .stall(stall));
    pipeline_latch DX(.pc_d(pc_fdreg), .pc_q(pc_dxreg), .ir_d(ir_pre_dxreg), .ir_q(ir_dxreg), .clock(clock),  
                        .dataA_d(dA_pre_dxreg), .dataB_d(data_readRegB), .dataA_q(da_dxreg), .dataB_q(db_dxreg), .reset(reset), .stall(1'b0));
    pipeline_latch XM(.pc_d(), .pc_q(), .ir_d(ir_pre_xmreg), .ir_q(ir_xmreg), .clock(clock),  
                        .dataA_d(dataA_pre_xmreg), .dataB_d(X_opB), .dataA_q(da_xmreg), .dataB_q(db_xmreg), .reset(reset), .stall(1'b0));
    pipeline_latch MW(.pc_d(), .pc_q(), .ir_d(ir_xmreg), .ir_q(ir_mwreg), .clock(clock),  
                        .dataA_d(da_xmreg), .dataB_d(q_dmem), .dataA_q(da_mwreg), .dataB_q(db_mwreg), .reset(reset), .stall(1'b0));
    // deconstructed multdiv P/W register for unique write_enable behavior
    register32 PW_P(.write(md_product), .write_enable(md_resultRDY), .read(da_pwreg), .clock(clock), .clear(reset | ctrl_MULT | ctrl_DIV));
    register32 PW_IR(.write(ir_dxreg), .write_enable(ctrl_DIV | ctrl_MULT), .read(ir_pwreg), .clock(clock), .clear(reset));

    /* -----Fetch stage----- */
    assign address_imem = pc_pcreg;
    cla_32 PC_ADD_1(.A(pc_pcreg), .B(32'b1), .Cin(1'b0), .S(pc_inc), .Cout());

    assign ir_pre_fdreg = (flush_fd) ? 32'b0 : q_imem;

    /* -----Decode stage----- */
    assign ctrl_readRegA = (is_bex_fd) ? 5'd30 : ir_fdreg[21:17];
    assign opcode_fd = ir_fdreg[31:27];
    assign is_sw_decode = (~opcode_fd[4] & ~opcode_fd[3] & opcode_fd[2] & opcode_fd[1] & opcode_fd[0]);  // store word
    assign is_branch_fd = (~opcode_fd[4] & ~opcode_fd[3] & opcode_fd[1] & ~opcode_fd[0]);
    assign is_jr_fd = (~opcode_fd[4] & ~opcode_fd[3] & opcode_fd[2] & ~opcode_fd[1] & ~opcode_fd[0]);  
    assign ctrl_readRegB = (is_sw_decode | is_branch_fd | is_jr_fd) ? ir_fdreg[26:22] : ir_fdreg[16:12];

    assign ir_pre_dxreg = (stall | flush_dx) ? 32'b0 : 
                          (do_jal) ? {5'b0, 5'd31, 22'b0} : // instruction to set reg 31 to current PC
                                            ir_fdreg;
    assign dA_pre_dxreg = (do_jal) ? pc_fdreg : data_readRegA;

    // TODO : might have to refactor bypassing to handle exceptions to alu_out in this case
    // TODO : add logic for bypassing an exception that happens at the ALU
    assign data_bypassed_readRegB = (readRegB_bypass_select == 2'd3) ? alu_out :
                                    (readRegB_bypass_select == 2'd2) ? da_xmreg :
                                    (readRegB_bypass_select == 2'd1) ? data_writeReg :
                                                                       data_readRegB;

    // bne
    assign do_bne = (~opcode_fd[4] & ~opcode_fd[3] & ~opcode_fd[2] & opcode_fd[1] & ~opcode_fd[0])
                    & (data_readRegA != data_bypassed_readRegB);
    
    // bex
    assign is_bex_fd = (opcode_fd[4] & ~opcode_fd[3] & opcode_fd[2] & opcode_fd[1] & ~opcode_fd[0]);
    assign bypassed_rstatus = (is_setx_dx) ? sx_imm_27_dx : (is_setx_xm) ? da_xmreg : data_readRegA;
    assign do_bex = is_bex_fd & (bypassed_rstatus != 0);
    
    // jump
    assign do_jump = (~opcode_fd[4] & ~opcode_fd[3] & ~opcode_fd[2] & ~opcode_fd[1] & opcode_fd[0]);
    assign do_jr = is_jr_fd;
    assign do_jal = (~opcode_fd[4] & ~opcode_fd[3] & ~opcode_fd[2] & opcode_fd[1] & opcode_fd[0]);

    // general branching hardware
    assign imm_17_fd = ir_fdreg[16:0];
    assign sx_imm_17_fd = {{15{imm_17_fd[16]}}, imm_17_fd};
    cla_32 bne_adder(.A(pc_fdreg), .B(sx_imm_17_fd), .Cin(1'b0), .S(pc_bne), .Cout());

    assign imm_27_fd = ir_fdreg[26:0];
    assign sx_imm_27_fd = {{5{imm_27_fd[26]}}, imm_27_fd};
    assign pc_jump = sx_imm_27_fd;
    assign pc_jr = data_bypassed_readRegB;

    // resolve bne, j, jal, jr in the decode stage
    // TODO:
    // [x] add sign extended 17-bit or select 27-bit (add a mux for that, check opcode) immediate to PC from F/D register
    //      [x] done for bne
    //      [x] done for bex
    // [x] add "bne opcode AND not equal" logic to determine if we branch
    // [x] add "bex opcode AND rstatus != 0" to check bex branch
    // [x] connect to the instruction flushes
    //      [x] done for bne
    //      [x] done for bex
    // [x] add new option to mux for PC reg write to choose the branch or jump PC
    //      [x] done for bne
    //      [x] done for bex
    // [x] add bypass into Decode from X, M, W for the registers used in branch comparisons
    // [ ] add new stall for lw -> branch instruction that needs the register

    // TODO:
    // [x] add support for j (jump to 26 bit immediate)
    // [x] add support for jal (writeback current PC to $r31)
    //      [x] do a jump, then overwrite IR field with "add PC to $r31" instruction
    // [ ] add support for jr (jump to address in $r31 directly)
    // [x] add support for bex and setx

    /* -----eXecute stage----- */
    assign imm_17_dx = ir_dxreg[16:0];
    assign sx_imm_17_dx = {{15{imm_17_dx[16]}}, imm_17_dx};
    assign imm_27_dx = ir_dxreg[26:0];
    assign sx_imm_27_dx = {{5{imm_27_dx[26]}}, imm_27_dx};

    assign is_setx_dx = (opcode_dx[4] & ~opcode_dx[3] & opcode_dx[2] & ~opcode_dx[1] & opcode_dx[0]);

    assign opcode_dx = ir_dxreg[31:27];
    assign is_imm_dx = (~opcode_dx[4] & ~opcode_dx[3] & opcode_dx[2] & opcode_dx[0]) 
                     | (~opcode_dx[4] & opcode_dx[3] & ~opcode_dx[2] & ~opcode_dx[1] & ~opcode_dx[0])
                     | is_setx_dx;
    assign alu_opcode = (is_imm_dx) ? 5'b00000: ir_dxreg[6:2]; // set opcode to add for addi, lw, sw
    assign alu_shamt = ir_dxreg[11:7];
    assign aluB_select = is_imm_dx; // i instruction(s)

    // X bypass logic
    assign X_opA = (aluA_bypass_select == 2'd0) ? da_xmreg : (aluA_bypass_select == 2'd1) ? data_writeReg : da_dxreg;
    assign X_opB = (aluB_bypass_select == 2'd0) ? da_xmreg : (aluB_bypass_select == 2'd1) ? data_writeReg : db_dxreg;

    // X branch (blt) logic
    assign is_blt_dx = (~opcode_dx[4] & ~opcode_dx[3] & opcode_dx[2] & opcode_dx[1] & ~opcode_dx[0]);
    cla_32 x_blt_adder(.A(pc_dxreg), .B(sx_imm_17_dx), .Cin(1'b0), .S(pc_blt), .Cout());
    assign do_blt = (alu_neq & ~alu_lt & is_blt_dx);

    // ALU
    assign alu_dopB =   (is_setx_dx) ? sx_imm_27_dx : 
                        (aluB_select) ? sx_imm_17_dx : X_opB;
    alu ALU(.data_operandA(X_opA), .data_operandB(alu_dopB), .ctrl_ALUopcode(alu_opcode), .ctrl_shiftamt(alu_shamt), 
            .data_result(alu_out), .isNotEqual(alu_neq), .isLessThan(alu_lt), .overflow(alu_overflow));

    // MultDiv
    assign ctrl_MULT = ~alu_opcode[4] & ~alu_opcode[3] & alu_opcode[2] & alu_opcode[1] & ~alu_opcode[0];
    assign ctrl_DIV = ~alu_opcode[4] & ~alu_opcode[3] & alu_opcode[2] & alu_opcode[1] & alu_opcode[0];

    multdiv MULTDIV(.data_operandA(X_opA), .data_operandB(X_opB), .ctrl_MULT(ctrl_MULT), .ctrl_DIV(ctrl_DIV), 
                    .clock(clock), .data_result(md_product), .data_exception(md_exception), .data_resultRDY(md_resultRDY));
    
    /*
     * BIG TODO : adjust $rstatus values depending on op that causes exception
     */

    /* -----Memory stage----- */
    assign opcode_xm = ir_xmreg[31:27];
    assign address_dmem = da_xmreg;
    assign data = (dmemD_bypass_select == 1'd1) ? db_xmreg : data_writeReg;
    assign wren = ~opcode_xm[4] & ~opcode_xm[3] & opcode_xm[2] & opcode_xm[1] & opcode_xm[0];
    assign is_setx_xm = (opcode_xm[4] & ~opcode_xm[3] & opcode_xm[2] & ~opcode_xm[1] & opcode_xm[0]);

    /* -----Writeback stage----- */
    assign writeReg_select = ~opcode_mw[4] & opcode_mw[3] & ~opcode_mw[2] & ~opcode_mw[1] & ~opcode_mw[0];
    assign data_writeReg = (md_resultRDY) ? md_product : (writeReg_select) ? db_mwreg : da_mwreg;
	assign ctrl_writeReg = (md_resultRDY) ? ir_pwreg[26:22] : ir_mwreg[26:22];
    assign opcode_mw = ir_mwreg[31:27];
    assign ctrl_writeEnable = (md_resultRDY) ? 1'b1 : (~opcode_mw[4] & ~opcode_mw[2] & ~opcode_mw[1] & ~opcode_mw[0]) | // R-type OR
                                (~opcode_mw[4] & ~opcode_mw[3] & opcode_mw[2] & ~opcode_mw[1] & opcode_mw[0]); // OR addi

    /* -----Bypass Logic----- */
    assign is_sw_dx = ~opcode_dx[4] & ~opcode_dx[3] & opcode_dx[2] & opcode_dx[1] & opcode_dx[0];
    assign is_sw_xm = ~opcode_xm[4] & ~opcode_xm[3] & opcode_xm[2] & opcode_xm[1] & opcode_xm[0];
    assign is_sw_mw = ~opcode_mw[4] & ~opcode_mw[3] & opcode_mw[2] & opcode_mw[1] & opcode_mw[0];
    assign rd_zero_dx = ~ir_dxreg[26] & ~ir_dxreg[25] & ~ir_dxreg[24] & ~ir_dxreg[23] & ~ir_dxreg[22];
    assign rd_zero_xm = ~ir_xmreg[26] & ~ir_xmreg[25] & ~ir_xmreg[24] & ~ir_xmreg[23] & ~ir_xmreg[22];
    assign rd_zero_mw = ~ir_mwreg[26] & ~ir_mwreg[25] & ~ir_mwreg[24] & ~ir_mwreg[23] & ~ir_mwreg[22];
    assign aluA_bypass_select = ((ir_dxreg[21:17] == ir_xmreg[26:22]) & ~rd_zero_xm & ~is_sw_xm) ? 2'd0 : 
                                ((ir_dxreg[21:17] == ir_mwreg[26:22]) & ~rd_zero_mw & ~is_sw_mw) ? 2'd1 : 
                                                                                   2'd2;
    assign is_r_type_dx = ~opcode_dx[4] & ~opcode_dx[3] & ~opcode_dx[2] & ~opcode_dx[1] & ~opcode_dx[0];
    assign is_branch_dx = ~opcode_dx[4] & ~opcode_dx[3] & opcode_dx[1] & ~opcode_dx[0];
    assign is_branch_xm = ~opcode_xm[4] & ~opcode_xm[3] & opcode_xm[1] & ~opcode_xm[0];
    assign is_branch_mw = ~opcode_mw[4] & ~opcode_mw[3] & opcode_mw[1] & ~opcode_mw[0];
    assign aluB_bypass_select = (((ir_dxreg[16:12] == ir_xmreg[26:22] & is_r_type_dx) | (ir_dxreg[26:22] == ir_xmreg[26:22] & is_branch_dx)) & ~rd_zero_xm & ~is_sw_xm & ~is_branch_xm) ? 2'd0 : 
                                (((ir_dxreg[16:12] == ir_mwreg[26:22] & is_r_type_dx) | (ir_dxreg[26:22] == ir_mwreg[26:22] & is_branch_dx)) & ~rd_zero_mw & ~is_sw_mw & ~is_branch_mw) ? 2'd1 : 
                                                                                                  2'd2;
    assign dmemD_bypass_select = ((ir_xmreg[26:22] == ir_mwreg[26:22]) & ctrl_writeEnable) ? 1'd0 : 1'd1;

    assign readRegB_bypass_select = ~(is_branch_fd | is_jr_fd) ? 2'd0 :
                                    (ir_fdreg[26:22] == ir_dxreg[26:22] & ~rd_zero_dx & ~is_sw_dx) ? 2'd3 :
                                    (ir_fdreg[26:22] == ir_xmreg[26:22] & ~rd_zero_xm & ~is_sw_xm) ? 2'd2 :
                                    (ir_fdreg[26:22] == ir_mwreg[26:22] & ~rd_zero_mw & ~is_sw_mw) ? 2'd1 :
                                                                                                     2'd0;

    /* -----Stall Logic----- */
    // DX IR is lw and (FD rs matches DX rd OR (FD rt matches DX rd and FD IR is not sw))
    // using slow method: stall ALL instructions from ctrl_MULT/DIV asserted until resultRDY asserted
    assign is_lw_dx = ~opcode_dx[4] & opcode_dx[3] & ~opcode_dx[2] & ~opcode_dx[1] & ~opcode_dx[0];
    assign is_sw_fd = ~opcode_fd[4] & ~opcode_fd[3] & opcode_fd[2] & opcode_fd[1] & opcode_fd[0];
    assign stall = (is_lw_dx & (ir_fdreg[21:17] == ir_dxreg[26:22] | (ir_fdreg[16:12] == ir_dxreg[26:22] & ~is_sw_fd) | 
                               (ir_fdreg[26:22] == ir_dxreg[26:22] & (is_branch_fd | is_jr_fd))
                    )) | md_stall;

    dffe_ref DELAY_HALF(resultRDY_shift_half, md_resultRDY, clock, 1'b1, 1'b0);
    dffe_ref DELAY_WHOLE(md_op_finished, resultRDY_shift_half, ~clock, 1'b1, 1'b0);
    dffe_ref MD_STALL(.q(md_stall), .d(ctrl_DIV | ctrl_MULT), .clk(~clock), .en(ctrl_DIV | ctrl_MULT), .clr((md_op_finished & ~ctrl_DIV & ~ctrl_MULT) | reset));
    
    /* -----Exception Bypassing Logic----- */
    // if there is alu_overflow in the ALU, change rd to 30, change result to 1
    assign write_rstatus = alu_overflow | is_setx_dx;
    assign ir_pre_xmreg = (flush_xm) ? 32'b0 : (write_rstatus) ? {5'b00000, 5'd30, ir_dxreg[21:0]} : ir_dxreg; // first term used to be ir_dxreg[31:27]
    assign dataA_pre_xmreg = (ctrl_DIV | ctrl_MULT | is_blt_dx) ? 32'b0 : (alu_overflow) ? 32'b1 : alu_out;

    /* -----Branching Logic----- */
    assign flush_xm = (do_blt);
    assign flush_dx = (do_blt);
    assign flush_fd = (do_blt | do_bne | do_bex | do_jump | do_jr | do_jal);
    assign next_pc = (do_blt) ? pc_blt :
                     (do_bne) ? pc_bne : 
                     (do_bex | do_jump | do_jal) ? pc_jump :
                     (do_jr) ? pc_jr :
                                pc_inc;

	/* END CODE */

endmodule

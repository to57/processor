module cla_8(A, B, Cin, S, G, P);

    input [7:0] A, B;
    input Cin;
    output [7:0] S;
    output G, P;

    wire [7:0] g, p, c;
    wire    w1_1, 
            w2_1, w2_2,
            w3_1, w3_2, w3_3,
            w4_1, w4_2, w4_3, w4_4,
            w5_1, w5_2, w5_3, w5_4, w5_5,
            w6_1, w6_2, w6_3, w6_4, w6_5, w6_6,
            w7_1, w7_2, w7_3, w7_4, w7_5, w7_6, w7_7,
            wg_1, wg_2, wg_3, wg_4, wg_5, wg_6, wg_7;

    assign c[0] = Cin;

    // generate g(enerate), p(ropagate) bits
    genvar i;
    generate
        for (i = 0; i < 8; i = i + 1) begin
            and gi(g[i], A[i], B[i]);
            or pi(p[i], A[i], B[i]);
        end
    endgenerate

    // Calculate carry bits 1-7 (c0 is Cin)
    // c1
    and a1_1(w1_1, p[0], c[0]);
    or o1(c[1], g[0], w1_1);

    // c2
    and a2_1(w2_1, p[1], p[0], c[0]);
    and a2_2(w2_2, p[1], g[0]);
    or o2(c[2], g[1], w2_1, w2_2);

    // c3
    and a3_1(w3_1, p[2], p[1], p[0], c[0]);
    and a3_2(w3_2, p[2], p[1], g[0]);
    and a3_3(w3_3, p[2], g[1]);
    or o3(c[3], g[2], w3_1, w3_2, w3_3);

    // c4
    and a4_1(w4_1, p[3], p[2], p[1], p[0], c[0]);
    and a4_2(w4_2, p[3], p[2], p[1], g[0]);
    and a4_3(w4_3, p[3], p[2], g[1]);
    and a4_4(w4_4, p[3], g[2]);
    or o4(c[4], g[3], w4_1, w4_2, w4_3, w4_4);

    // c5
    and a5_1(w5_1, p[4], p[3], p[2], p[1], p[0], c[0]);
    and a5_2(w5_2, p[4], p[3], p[2], p[1], g[0]);
    and a5_3(w5_3, p[4], p[3], p[2], g[1]);
    and a5_4(w5_4, p[4], p[3], g[2]);
    and a5_5(w5_5, p[4], g[3]);
    or o5(c[5], g[4], w5_1, w5_2, w5_3, w5_4, w5_5);

    // c6
    and a6_1(w6_1, p[5], p[4], p[3], p[2], p[1], p[0], c[0]);
    and a6_2(w6_2, p[5], p[4], p[3], p[2], p[1], g[0]);
    and a6_3(w6_3, p[5], p[4], p[3], p[2], g[1]);
    and a6_4(w6_4, p[5], p[4], p[3], g[2]);
    and a6_5(w6_5, p[5], p[4], g[3]);
    and a6_6(w6_6, p[5], g[4]);
    or o6(c[6], g[5], w6_1, w6_2, w6_3, w6_4, w6_5, w6_6);

    // c7
    and a7_1(w7_1, p[6], p[5], p[4], p[3], p[2], p[1], p[0], c[0]);
    and a7_2(w7_2, p[6], p[5], p[4], p[3], p[2], p[1], g[0]);
    and a7_3(w7_3, p[6], p[5], p[4], p[3], p[2], g[1]);
    and a7_4(w7_4, p[6], p[5], p[4], p[3], g[2]);
    and a7_5(w7_5, p[6], p[5], p[4], g[3]);
    and a7_6(w7_6, p[6], p[5], g[4]);
    and a7_7(w7_7, p[6], g[5]);
    or o7(c[7], g[6], w7_1, w7_2, w7_3, w7_4, w7_5, w7_6, w7_7);

    // xor carries, A, B to get sum bits
    genvar j;
    generate
        for (j = 0; j < 8; j = j + 1) begin
            xor sumj(S[j], A[j], B[j], c[j]);
        end
    endgenerate

    // generate block level propagate and generate bits
    and Pblock(P, p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);
    and Gand_1(wg_1, p[7], g[6]);
    and Gand_2(wg_2, p[7], p[6], g[5]);
    and Gand_3(wg_3, p[7], p[6], p[5], g[4]);
    and Gand_4(wg_4, p[7], p[6], p[5], p[4], g[3]);
    and Gand_5(wg_5, p[7], p[6], p[5], p[4], p[3], g[2]);
    and Gand_6(wg_6, p[7], p[6], p[5], p[4], p[3], p[2], g[1]);
    and Gand_7(wg_7, p[7], p[6], p[5], p[4], p[3], p[2], p[1], g[0]);
    or Gblock(G, g[7], wg_1, wg_2, wg_3, wg_4, wg_5, wg_6, wg_7);

endmodule
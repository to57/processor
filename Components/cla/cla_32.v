module cla_32(A, B, Cin, S, Cout);

    input [31:0] A, B;
    input Cin;
    output [31:0] S;
    output Cout;

    wire [3:0] P, G;
    wire c8, c16, c24, c32;
    wire    w8_1,
            w16_1, w16_2,
            w24_1, w24_2, w24_3,
            w32_1, w32_2, w32_3, w32_4;
    assign Cout = c32;

    // four 8-bit CLA modules
    cla_8 cla0(.A(A[7:0]), .B(B[7:0]), .Cin(Cin), .S(S[7:0]), .G(G[0]), .P(P[0]));
    cla_8 cla1(.A(A[15:8]), .B(B[15:8]), .Cin(c8), .S(S[15:8]), .G(G[1]), .P(P[1]));
    cla_8 cla2(.A(A[23:16]), .B(B[23:16]), .Cin(c16), .S(S[23:16]), .G(G[2]), .P(P[2]));
    cla_8 cla3(.A(A[31:24]), .B(B[31:24]), .Cin(c24), .S(S[31:24]), .G(G[3]), .P(P[3]));

    // Calculate carry bits c8, c16, c24, c32
    // c8
    and a8_1(w8_1, P[0], Cin);
    or o8_1(c8, G[0], w8_1);

    // c16
    and a16_1(w16_1, P[1], P[0], Cin);
    and a16_2(w16_2, P[1], G[0]);
    or o16_1(c16, G[1], w16_1, w16_2);

    // c24
    and a24_1(w24_1, P[2], P[1], P[0], Cin);
    and a24_2(w24_2, P[2], P[1], G[0]);
    and a24_3(w24_3, P[2], G[1]);
    or o24_1(c24, G[2], w24_1, w24_2, w24_3);

    // c32
    and a32_1(w32_1, P[3], P[2], P[1], P[0], Cin);
    and a32_2(w32_2, P[3], P[2], P[1], G[0]);
    and a32_3(w32_3, P[3], P[2], G[1]);
    and a32_4(w32_4, P[3], G[2]);
    or o32_1(c32, G[3], w32_1, w32_2, w32_3, w32_4);

endmodule
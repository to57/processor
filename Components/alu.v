module alu(data_operandA, data_operandB, ctrl_ALUopcode, 
            ctrl_shiftamt, data_result, isNotEqual, isLessThan, 
            overflow);
        
    input [31:0] data_operandA, data_operandB;
    input [4:0] ctrl_ALUopcode, ctrl_shiftamt;

    output [31:0] data_result;
    output isNotEqual, isLessThan, overflow;

    wire [31:0] sum, a_and_b, a_or_b, a_xor_b, not_b, l_shifted, r_shifted, add_op_b;
    wire sub, Cout, ovf_or_1, ovf_or_2, not_Asign, not_Bsign, not_Sumsign, lt_and;
    wire lt_term_1, lt_subterm_1, lt_subterm_2, lt_subterm, lt_term_2;

    // logical operations and shifts
    and_32 A_AND_B(.A(data_operandA), .B(data_operandB), .result(a_and_b));
    or_32 A_OR_B(.A(data_operandA), .B(data_operandB), .result(a_or_b));
    not_32 NOT_B(.A(data_operandB), .result(not_b));
    barrel_l SLL(.in(data_operandA), .shamt(ctrl_shiftamt), .sh(l_shifted));
    barrel_r SRA(.in(data_operandA), .shamt(ctrl_shiftamt), .sh(r_shifted));

    // addition with support for subtraction
    assign sub = ctrl_ALUopcode[0];
    mux_2 ADD_OP_B(add_op_b, sub, data_operandB, not_b);
    cla_32 adder(.A(data_operandA), .B(add_op_b), .Cin(sub), .S(sum), .Cout(Cout));

    // comparisons
    and LTand1(lt_term_1, data_operandA[31], not_b[31]);
    and LTand2(lt_subterm_1, data_operandA[31], data_operandB[31]);
    and LTand3(lt_subterm_2, not_Asign, not_b[31]);
    or LTor1(lt_subterm, lt_subterm_1, lt_subterm_2);
    and LTand4(lt_term_2, lt_subterm, sum[31]);
    or LT(isLessThan, lt_term_1, lt_term_2);

    or NEQ(isNotEqual, sum[0], sum[1], sum[2], sum[3], sum[4], sum[5], sum[6], sum[7], 
                        sum[8], sum[9], sum[10], sum[11], sum[12], sum[13], sum[14], sum[15],
                        sum[16], sum[17], sum[18], sum[19], sum[20], sum[21], sum[22], sum[23],
                        sum[24], sum[25], sum[26], sum[27], sum[28], sum[29], sum[30], sum[31]);
    
    // overflow detection
    not NOT_Asign(not_Asign, data_operandA[31]);
    not NOT_Bsign(not_Bsign, add_op_b[31]);
    not NOT_Sumsign(not_Sumsign, sum[31]);
    and OVF_AND_1(ovf_or_1, not_Asign, not_Bsign, sum[31]);
    and OVF_AND_2(ovf_or_2, data_operandA[31], add_op_b[31], not_Sumsign);
    or ovf(overflow, ovf_or_1, ovf_or_2);

    mux_8 output_mux(data_result, ctrl_ALUopcode[2:0], sum, sum, a_and_b, a_or_b, l_shifted, r_shifted, 32'bx, 32'bx);

endmodule
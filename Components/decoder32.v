module decoder32(out, select, enable);
    input [4:0] select;
    input enable;
    output [31:0] out;

    wire [31:0] enable_ext;
    assign enable_ext = {31'b0, enable};

    barrel_l shifter(enable_ext, select, out);
endmodule

module divider(  data_operandA, data_operandB, ctrl_DIV, clock, 
                    data_result, data_exception, data_resultRDY );

    // Dividend is A, divisor is B
    input [31:0] data_operandA, data_operandB;
    input clock, ctrl_DIV;

    output [31:0] data_result;
    output data_exception, data_resultRDY;

    // steps for dividing:
    // to handle negative numbers:
    //      if divisor or dividend is negative, invert sign
    //      store original sign of dividend/divisor
    // initialize:
    //      N = bits in dividend = 32
    //      A = 0
    //      M = divisor
    //      Q = dividend
    // repeat until N = 0:
    //      if sign bit of A is 0:
    //          shift AQ left, A = A-M
    //      else:
    //          shift AQ left, A = A+M
    //      if sign bit of A is 0:
    //          Q[0] = 1
    //      else:
    //          Q[0] = 0
    //      N = N-1
    // if sign bit of A is 1:
    //      A = A+M
    // quotient is in register Q
    // remainder is in register A
    // change sign of Q and A to match original signs of operands
    // assert result ready

    wire [31:0] divisor, counter, counter_inc, adder_result, 
                dividend_inverted, divisor_inverted, dividend_write, divisor_write, Q_inverted, data_pre_result;
    wire [63:0] AQ, AQ_shifted, AQ_added, AQ_write;
    wire adder_carry, AQ_write_en, sub, divisor_sign, dividend_sign, ctrl_START_DIV, Q_sign, 
            data_result_done, data_resultRDY_shift_half, ctrl_shift_half, _Cout1, _Cout2, Cout, _Cout3;

    // delay the ctrl_DIV signal by one clock cycle to allow time to invert
    dffe_ref DELAY_HALF(ctrl_shift_half, ctrl_DIV, clock, 1'b1, 1'b0);
    dffe_ref DELAY_WHOLE(ctrl_START_DIV, ctrl_shift_half, ~clock, 1'b1, ctrl_DIV);

    // store the original sign of dividend/divisor
    dffe_ref DIVIDEND_SIGN(dividend_sign, data_operandA[31], ctrl_DIV, 1'b1, 1'b0);
    dffe_ref DIVISOR_SIGN(divisor_sign, data_operandB[31], ctrl_DIV, 1'b1, 1'b0);
    cla_32 DIVIDEND_INVERTER(~data_operandA, 32'b1, 1'b0, dividend_inverted, _Cout1);
    cla_32 DIVISOR_INVERTER(~data_operandB, 32'b1, 1'b0, divisor_inverted, _Cout2);
    assign dividend_write = (dividend_sign) ? dividend_inverted : data_operandA;
    assign divisor_write = (divisor_sign) ? divisor_inverted : data_operandB;

    assign AQ_shifted = AQ << 1'b1;
    assign AQ_added = {adder_result, AQ_shifted[31:0]};
    assign AQ_write = (ctrl_START_DIV) ?  {32'b0, dividend_write} : {AQ_added[63:1], ~AQ_added[63]};
    assign AQ_write_en = ~data_result_done;

    register32 DIVISOR_REG(divisor_write, ctrl_START_DIV, divisor, clock, 1'b0);
    register64 AQ_REG(AQ_write, AQ_write_en, AQ, clock, 1'b0);

    adder ADDER(AQ_shifted[63:32], divisor, sub, adder_result, adder_carry);

    register32 COUNTER(counter_inc, 1'b1, counter, clock, ctrl_START_DIV);
    cla_32 COUNTER_ADDER(counter, 32'b1, 1'b0, counter_inc, Cout);

    assign Q_sign = dividend_sign ^ divisor_sign;
    cla_32 QUOTIENT_INVERTER(~AQ[31:0], 32'b1, 1'b0, Q_inverted, _Cout3);

    // control logic
    assign sub = ~AQ[63];
    assign data_result_done = counter[5]; // ready at 32 cycles
    dffe_ref RDY_DELAY_HALF(data_resultRDY_shift_half, data_result_done, clock, 1'b1, ctrl_DIV);
    dffe_ref RDY_DELAY_WHOLE(data_resultRDY, data_resultRDY_shift_half, ~clock, 1'b1, ctrl_DIV);
    
    assign data_exception = ~|divisor; // exception if dividing by 0
    assign data_pre_result = (Q_sign) ? Q_inverted : AQ[31:0];
    assign data_result = (data_exception) ? 0 : data_pre_result;

endmodule
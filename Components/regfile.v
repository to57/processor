module regfile (
	clock,
	ctrl_writeEnable, ctrl_reset, ctrl_writeReg,
	ctrl_readRegA, ctrl_readRegB, data_writeReg,
	data_readRegA, data_readRegB
);

	input clock, ctrl_writeEnable, ctrl_reset;
	input [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	input [31:0] data_writeReg;

	output [31:0] data_readRegA, data_readRegB;

	wire [31:0] decoded_writeReg, decoded_readRegA, decoded_readRegB;
	wire [31:0] reads [31:0];

	decoder32 decodeWrite(decoded_writeReg, ctrl_writeReg, ctrl_writeEnable);
	decoder32 decodeReadA(decoded_readRegA, ctrl_readRegA, 1'b1);
	decoder32 decodeReadB(decoded_readRegB, ctrl_readRegB, 1'b1);

	assign reads[0] = 32'b0;

	genvar i;
	generate
		for (i = 1; i < 32; i = i + 1) begin
			register32 register(data_writeReg, decoded_writeReg[i], reads[i], ~clock, ctrl_reset);
		end
		for (i = 0; i < 32; i = i + 1) begin
			assign data_readRegA = (decoded_readRegA[i]) ? reads[i] : 32'bz;
			assign data_readRegB = (decoded_readRegB[i]) ? reads[i] : 32'bz;
		end
	endgenerate

endmodule

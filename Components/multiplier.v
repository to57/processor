module multiplier(  data_operandA, data_operandB, ctrl_MULT, clock, 
                    data_result, data_exception, data_resultRDY );

    // Multiplicand is A, multiplier is B
    input [31:0] data_operandA, data_operandB;
    input clock, ctrl_MULT;

    output [31:0] data_result;
    output data_exception, data_resultRDY;

    wire [31:0] multiplicand, adder_operand_B, counter_inc, counter, sig_bits,
                adder_result, product_top32, product_bot32;
    wire [63:0] product, product_write, product_pre_shift, product_shifted;
    wire    adder_carry, clear_counter, 
            sub, noop, product_write_en, mult_low_bit, high_sig_bits, sign_ovf,
            a_zero, b_zero, sign_extend_bit, Cout;

    assign product_pre_shift = {adder_result, product_bot32};
    assign product_shifted = product_pre_shift >>> 1'b1;
    assign sign_extend_bit = ( sub & adder_result[31] & ~(|adder_result[30:0])) ? 1'b0 : product_pre_shift[63];
    assign product_write = (ctrl_MULT) ? {data_operandB, 32'b0} >>> 8'd32 : {sign_extend_bit, product_shifted[62:0]};
    assign {product_top32, product_bot32} = product;
    assign product_write_en = ~data_resultRDY;

    register32 MULTIPLICAND_REG(data_operandA, ctrl_MULT, multiplicand, clock, 1'b0);
    register64 PRODUCT_REG(product_write, product_write_en, product, clock, 1'b0);
    dffe_ref MULTIPLIER_LOW_BIT(mult_low_bit, product_bot32[0], clock, 1'b1, ctrl_MULT);

    assign adder_operand_B = (noop) ? 32'b0 : multiplicand;
    adder ADDER(product_top32, adder_operand_B, sub, adder_result, adder_carry);

    register32 COUNTER(counter_inc, 1'b1, counter, clock, ctrl_MULT);
    cla_32 COUNTER_ADDER(counter, 32'b1, 1'b0, counter_inc, Cout);

    // control logic
    assign noop = (product_bot32[0] & mult_low_bit) | (~product_bot32[0] & ~mult_low_bit);
    assign sub = product_bot32[0];
    assign data_resultRDY = counter[5]; // ready at 32 cycles (31 incremented)
    assign data_result = product_bot32;

    // overflow detection
    // check for significant bits in upper half of product register
    genvar i;
    generate
        for (i = 0; i < 32; i = i + 1) begin
            xor SIGBIT_I(sig_bits[i], product_top32[i], product_bot32[31]);
        end
    endgenerate

    assign high_sig_bits = |sig_bits;
    
    // check to see if signs of operands and product make sense
    // if either operand is 0, there is no sign overflow

    xor SIGN_CHECK(sign_ovf, data_operandA[31], data_operandB[31], data_result[31]);
    nor A_ZERO(a_zero, 
                data_operandA[0], data_operandA[1], data_operandA[2], data_operandA[3], data_operandA[4], 
                data_operandA[5], data_operandA[6], data_operandA[7], data_operandA[8], data_operandA[9], 
                data_operandA[10], data_operandA[11], data_operandA[12], data_operandA[13], data_operandA[14], 
                data_operandA[15], data_operandA[16], data_operandA[17], data_operandA[18], data_operandA[19], 
                data_operandA[20], data_operandA[21], data_operandA[22], data_operandA[23], data_operandA[24], 
                data_operandA[25], data_operandA[26], data_operandA[27], data_operandA[28], data_operandA[29], 
                data_operandA[30], data_operandA[31]);
    nor B_ZERO(b_zero, 
                data_operandB[0], data_operandB[1], data_operandB[2], data_operandB[3], data_operandB[4], 
                data_operandB[5], data_operandB[6], data_operandB[7], data_operandB[8], data_operandB[9], 
                data_operandB[10], data_operandB[11], data_operandB[12], data_operandB[13], data_operandB[14], 
                data_operandB[15], data_operandB[16], data_operandB[17], data_operandB[18], data_operandB[19], 
                data_operandB[20], data_operandB[21], data_operandB[22], data_operandB[23], data_operandB[24], 
                data_operandB[25], data_operandB[26], data_operandB[27], data_operandB[28], data_operandB[29], 
                data_operandB[30], data_operandB[31]);

    assign data_exception = (sign_ovf & ~a_zero & ~b_zero) | high_sig_bits;

    // Repeat the following 16 times with clock cycle:
    // - Multiplier LSBs == 10? Subtract multiplicand from product
    // - Multiplier LSBs == 01? Add multiplicand to product
    // - Multiplier LSBs == 00/11? Do nothing
    // Shift product/multiplier right by 1

    // How to do this?
    // I have up to 100 cycles between the start and end of multiplying
    // CTRL_MULT is asserted from a falling edge to falling edge
    // so I want to take in inputs on the rising edge that ctrl_MULT is asserted
    // then, do an operation on each following rising edge
    // when the results are ready, assert data_ready on a rising edge
    // then take it back down on the following rising edge

    // on each rising edge (after ctrl_MULT is finished being asserted)
    // I do an add and store the result in product reg
    // on the following falling edge I shift the reg over one and bring in the
    // carry bit from the last add, which I need to latch/store in flip flop
    
    // Order of events:
    // ctrl_MULT is asserted AND rising edge
    //      read in data operands and initialize registers
    //      set our counter to 0
    //
    // counter is less than 32 AND ctrl_MULT is not asserted AND rising edge
    //      store the result of adder in product reg
    //      latch Cout
    // counter is less than 32 AND ctrl_MULT is not asserted AND falling edge
    //      shift the product reg and bring in the carry bit
    //      increment counter
    //
    // counter is 32 AND ctrl_MULT is not asserted AND rising edge
    //      assert and latch data_resultRDY
    // data_resultRDY AND rising edge
    //      set data_resultRDY to 0 and latch it again
    //      indicate that op is over

endmodule
module register64(write, write_enable, read, clock, clear);

    input [63:0] write;
    input write_enable, clock, clear;

    output [63:0] read;

    genvar i;
    generate
        for (i = 0; i < 64; i = i + 1) begin
            // DFFE to store one bit of data
            dffe_ref bit_i(read[i], write[i], clock, write_enable, clear);
        end
    endgenerate

endmodule
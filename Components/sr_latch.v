module sr_latch(input S, input R, output Q, output Qbar);
    nor n1(Q, R, Qbar);
    nor n2(Qbar, S, Q);
endmodule
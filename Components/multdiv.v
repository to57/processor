module multdiv(
	data_operandA, data_operandB, 
	ctrl_MULT, ctrl_DIV, 
	clock, 
	data_result, data_exception, data_resultRDY);

    input [31:0] data_operandA, data_operandB;
    input ctrl_MULT, ctrl_DIV, clock;

    output [31:0] data_result;
    output data_exception, data_resultRDY;

    wire is_DIV, is_MULT, mult_resultRDY, div_resultRDY, mult_exception, div_exception, active, notActive, op_finished, resultRDY_shift_half;
    wire [31:0] mult_result, div_result, op_A, op_B;

    register32 OP_A(data_operandA, (ctrl_DIV | ctrl_MULT), op_A, ~clock, 1'b0);
    register32 OP_B(data_operandB, (ctrl_DIV | ctrl_MULT), op_B, ~clock, 1'b0);

    // DFFE latch holds whether we're doing DIV or MULT
    // nor SET(is_DIV, is_MULT, ctrl_MULT);
    // nor RESET(is_MULT, is_DIV, ctrl_DIV);
    dffe_ref IS_DIV(.q(is_DIV), .d(ctrl_DIV), .clk(~clock), .en(ctrl_DIV), .clr(ctrl_MULT));
    assign is_MULT = ~is_DIV;


    // one clock cycle after resultRDY, multdiv is no longer active if neither ctrl_DIV or ctrl_MULT is asserted
    dffe_ref DELAY_HALF(resultRDY_shift_half, data_resultRDY, clock, 1'b1, 1'b0);
    dffe_ref DELAY_WHOLE(op_finished, resultRDY_shift_half, ~clock, 1'b1, 1'b0);
    // assign active = 1'b0;
    dffe_ref MD_ACTIVE(.q(active), .d(ctrl_DIV | ctrl_MULT), .clk(clock), 
                        .en(ctrl_DIV | ctrl_MULT), .clr(op_finished & ~ctrl_DIV & ~ctrl_MULT));

    multiplier MULT(op_A, op_B, ctrl_MULT, clock, mult_result, mult_exception, mult_resultRDY);
    divider DIV(op_A, op_B, ctrl_DIV, clock, div_result, div_exception, div_resultRDY);

    assign data_result = (~active) ? 32'b0 : (is_DIV) ? div_result : mult_result;
    assign data_exception = (~active | ~data_resultRDY) ? 1'b0 : (is_DIV) ? div_exception : mult_exception;
    assign data_resultRDY = (~active) ? 1'b0 : (is_DIV) ? div_resultRDY : mult_resultRDY;

endmodule
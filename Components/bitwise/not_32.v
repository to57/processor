module not_32(A, result);

    input [31:0] A;
    output [31:0] result;

    not NOT0(result[0], A[0]);
    not NOT1(result[1], A[1]);
    not NOT2(result[2], A[2]);
    not NOT3(result[3], A[3]);
    not NOT4(result[4], A[4]);
    not NOT5(result[5], A[5]);
    not NOT6(result[6], A[6]);
    not NOT7(result[7], A[7]);
    not NOT8(result[8], A[8]);
    not NOT9(result[9], A[9]);
    not NOT10(result[10], A[10]);
    not NOT11(result[11], A[11]);
    not NOT12(result[12], A[12]);
    not NOT13(result[13], A[13]);
    not NOT14(result[14], A[14]);
    not NOT15(result[15], A[15]);
    not NOT16(result[16], A[16]);
    not NOT17(result[17], A[17]);
    not NOT18(result[18], A[18]);
    not NOT19(result[19], A[19]);
    not NOT20(result[20], A[20]);
    not NOT21(result[21], A[21]);
    not NOT22(result[22], A[22]);
    not NOT23(result[23], A[23]);
    not NOT24(result[24], A[24]);
    not NOT25(result[25], A[25]);
    not NOT26(result[26], A[26]);
    not NOT27(result[27], A[27]);
    not NOT28(result[28], A[28]);
    not NOT29(result[29], A[29]);
    not NOT30(result[30], A[30]);
    not NOT31(result[31], A[31]);


endmodule
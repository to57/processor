module xor_8(A, B, result);

    input [7:0] A, B;
    output [7:0] result;

    xor XOR0(result[0], A[0], B[0]);
    xor XOR1(result[1], A[1], B[1]);
    xor XOR2(result[2], A[2], B[2]);
    xor XOR3(result[3], A[3], B[3]);
    xor XOR4(result[4], A[4], B[4]);
    xor XOR5(result[5], A[5], B[5]);
    xor XOR6(result[6], A[6], B[6]);
    xor XOR7(result[7], A[7], B[7]);

endmodule
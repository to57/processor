module and_8(result, A, B);

    input [7:0] A, B;
    output [7:0] result;

    and AND0(result[0], A[0], B[0]);
    and AND1(result[1], A[1], B[1]);
    and AND2(result[2], A[2], B[2]);
    and AND3(result[3], A[3], B[3]);
    and AND4(result[4], A[4], B[4]);
    and AND5(result[5], A[5], B[5]);
    and AND6(result[6], A[6], B[6]);
    and AND7(result[7], A[7], B[7]);

endmodule
module xor_32(result, A, B);

    input [31:0] A, B;
    output [31:0] result;

    xor_8 XOR0(.A(A[7:0]), .B(B[7:0]), .result(result[7:0]));
    xor_8 XOR1(.A(A[15:8]), .B(B[15:8]), .result(result[15:8]));
    xor_8 XOR2(.A(A[23:16]), .B(B[23:16]), .result(result[23:16]));
    xor_8 XOR3(.A(A[31:24]), .B(B[31:24]), .result(result[31:24]));

endmodule
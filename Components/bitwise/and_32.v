module and_32(result, A, B);

    input [31:0] A, B;
    output [31:0] result;

    and_8 AND0(.A(A[7:0]), .B(B[7:0]), .result(result[7:0]));
    and_8 AND1(.A(A[15:8]), .B(B[15:8]), .result(result[15:8]));
    and_8 AND2(.A(A[23:16]), .B(B[23:16]), .result(result[23:16]));
    and_8 AND3(.A(A[31:24]), .B(B[31:24]), .result(result[31:24]));

endmodule
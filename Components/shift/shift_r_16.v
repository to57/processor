module shift_r_16(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 0; i < 16; i = i + 1) begin
            assign sh[i] = in[i+16];
        end
    endgenerate

    genvar j;
    generate
        for (j = 16; j < 32; j = j + 1) begin
            assign sh[j] = in[31];
        end
    endgenerate

endmodule
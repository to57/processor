module barrel_r(in, shamt, sh);

    input [31:0] in;
    input [4:0] shamt;
    output [31:0] sh;

    wire [31:0] w1, w2, w4, w8, w16;
    wire [31:0] s1, s2, s4, s8, s16;

    shift_r_1 shift1(in, s1);
    shift_r_2 shift2(w1, s2);
    shift_r_4 shift4(w2, s4);
    shift_r_8 shift8(w4, s8);
    shift_r_16 shift16(w8, s16);

    mux_2 shift_1(w1, shamt[0], in, s1);
    mux_2 shift_2(w2, shamt[1], w1, s2);
    mux_2 shift_4(w4, shamt[2], w2, s4);
    mux_2 shift_8(w8, shamt[3], w4, s8);
    mux_2 shift_16(w16, shamt[4], w8, s16);

    assign sh = w16;

endmodule
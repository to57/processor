module shift_r_8(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 0; i < 24; i = i + 1) begin
            assign sh[i] = in[i+8];
        end
    endgenerate

    genvar j;
    generate
        for (j = 24; j < 32; j = j + 1) begin
            assign sh[j] = in[31];
        end
    endgenerate

endmodule
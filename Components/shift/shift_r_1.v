module shift_r_1(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 0; i < 31; i = i + 1) begin
            assign sh[i] = in[i+1];
        end
    endgenerate

    // sign extension
    assign sh[31] = in[31];

endmodule
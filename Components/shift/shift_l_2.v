module shift_l_2(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 2; i < 32; i = i + 1) begin
            assign sh[i] = in[i-2];
        end
    endgenerate

    assign sh[0] = 0;
    assign sh[1] = 0;

endmodule
module shift_r_2(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 0; i < 30; i = i + 1) begin
            assign sh[i] = in[i+2];
        end
    endgenerate

    assign sh[31] = in[31];
    assign sh[30] = in[31];

endmodule
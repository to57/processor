module shift_r_4(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 0; i < 28; i = i + 1) begin
            assign sh[i] = in[i+4];
        end
    endgenerate

    genvar j;
    generate
        for (j = 28; j < 32; j = j + 1) begin
            assign sh[j] = in[31];
        end
    endgenerate

endmodule
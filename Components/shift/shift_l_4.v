module shift_l_4(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 4; i < 32; i = i + 1) begin
            assign sh[i] = in[i-4];
        end
    endgenerate

    genvar j;
    generate
        for (j = 0; j < 4; j = j + 1) begin
            assign sh[j] = 0;
        end
    endgenerate

endmodule
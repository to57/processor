module shift_l_1(in, sh);

    input [31:0] in;
    output [31:0] sh;

    genvar i;

    generate
        for (i = 1; i < 32; i = i + 1) begin
            assign sh[i] = in[i-1];
        end
    endgenerate

    assign sh[0] = 0;

endmodule
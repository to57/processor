module adder(A, B, SUB, Result, Cout);
    input [31:0] A, B;
    input SUB;

    output [31:0] Result;
    output Cout;

    wire[31:0] operand_B;
    wire Cin;

    assign operand_B = (SUB) ? ~B : B;
    assign Cin = SUB;

    cla_32 CLA(A, operand_B, Cin, Result, Cout);
endmodule
module register32(write, write_enable, read, clock, clear);

    input [31:0] write;
    input write_enable, clock, clear;

    output [31:0] read;

    genvar i;
    generate
        for (i = 0; i < 32; i = i + 1) begin
            // DFFE to store one bit of data
            dffe_ref bit_i(read[i], write[i], clock, write_enable, clear);
        end
    endgenerate

endmodule